function VelocityBrush() {

    this.regexList = [
        /** Escaped Content **/
        {
            regex: /#\[\[[\s\S]*]]#/gm,
            css: 'plain'
        },
        /** Comments **/
        {
            // In-line comments
            regex: /##.*/g,
            css: 'comments'
        },
        {
            // Multi-line comments
            // TODO: Needs some work. Doesn't correctly detect when comment tags are embedded within a comment.
            regex: /#\*[\s\S]*?\*#/gm,
            css: 'comments'
        },
        /** Escaped Keywords **/
        {
            regex: /\\(#set|#macro|#end|#foreach|#if|#else|#elseif|#include|#parse|#define|#evaluate|#stop|#break)/g,
            css: 'plain'
        },
        /** Keywords **/
        {
            regex: /(#set|#macro|#end|#foreach|#if|#else|#elseif|#include|#parse|#define|#evaluate|#stop|#break)/g,
            css: 'keyword'
        },
        /** Escaped Variables **/
        {
            regex: /\\\$!?(\{[a-zA-Z0-9\-_]+\}|[a-zA-Z0-9\-_]+)/g,
            css: 'plain'
        },
        /** Variables **/
        {
            // - Don't match if there's a leading forward slash ('\')
            // - Must start with dollar sign ('$')
            // - Optional exclamation mark immediately after dollar sign ('!')
            // - Optional left curly brace before variable name ('{'). Mandatory if ending right curly brace is present.
            // - Valid variable characters: A-Z (upper and lower case), 0-9, hyphen and underscore
            // - Optional right curly brace after viable name ('}'). Mandatory if leading left curly brace is present.
            regex: /\$!?(\{[a-zA-Z0-9\-_]+\}|[a-zA-Z0-9\-_]+)/g,
            css: 'variable'
        }];

    this.forHtmlScript({
        left	: /(&lt;|<)%[@!=]?/g,
        right	: /%(&gt;|>)/g
    });


}
VelocityBrush.prototype	= new SyntaxHighlighter.Highlighter();
VelocityBrush.aliases = ['velocity', 'vm', 'vmd', 'vtl'];


SyntaxHighlighter.brushes.Velocity = VelocityBrush;


